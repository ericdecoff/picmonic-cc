# picmonic-cc

## Requirements

```
Vagrant relies on interactions with 3rd party systems, known as
"providers", to provide Vagrant with resources to run development
environments. Examples are VirtualBox, VMware, Hyper-V.
```

For this project | | Version
----- | ------ | ----- |
VirtualBox | https://www.virtualbox.org/wiki/Downloads | 5.1.1
Vagrant | [https://www.vagrantup.com/downloads.html](https://www.vagrantup.com/downloads.html) | 1.9.0
Homestead | [https://laravel.com/docs/5.3/homestead](https://laravel.com/docs/5.3/homestead) | 1.2.4
CygWin | [https://www.cygwin.com/](https://www.cygwin.com/) | 2.6.0

## Verify vagrant installation
``` 
vagrant -v 
```

## Project Notes:

| | |
| ----- | ----- | ----- |
| Create Project Directory | mkdir %userprofile%\Documents\picmonic-cc | |
| Change Directory | cd %userprofile%\Documents\picmonic-cc | |
| Clone Homestead | git clone https://github.com/laravel/homestead.git homestead | |
| Change Directory | cd homestead | |
| Initialize Homestead | init.bat | |
| Edit Homestead.yaml | vi %userprofile%/.homestead/Homestead.yaml | |
| Change Folders Section  | folders: | | 
| | - map:~/Documents/picmonic-cc/laravel | | 
| |   to:/home/vagrant/laravel | |
| Change Sites Section  | sites: | | 
| | - map:index.php | | 
| |   to:/home/vagrant/laravel/public | |



## Start vagrant 
``` 
vagrant up 
```

## Connect vagrant box
```
// NOTE: cygwin does not install openssh by default [ re-run cygwin setup and search openssh in packages and added it ] 
vagrant ssh
```

## Create initial laravel project
```
composer create-project laravel/laravel laravel
```

## Test Site
| | |
| ----- | ----- | ----- |
| Default Site |  [ http://192.168.10.10 ](http://192.168.10.10) | |